import React, {Component} from 'react';
import { Table, thead, tbody, Nav, NavItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Navbar from './CustomNavbar';
import Tabeltiket from './TableTiket';

export default class Tiket extends Component{
    componentDidMount(){
        document.title = "Dashboard Tiket"
      }
    render(){
        return(
          <div>
              <Navbar/>
              <Tabeltiket/>
        </div>
            );
    }
}