import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Navbar from './CustomNavbar';

import TableSubscribe from './TableSubscribe';
export default class LihatSub extends Component{
    componentDidMount(){
        document.title = "Hallo Subscribe"
    }
    render(){
        return(
            <div>
               <Navbar/>
               <TableSubscribe/>
            </div>
            );
    }
}