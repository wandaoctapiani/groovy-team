import React, {Component} from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./LoginCard.js";

class Index extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Login}/>
        </div>
      </Router>
    );
  }
}

export default Index;