import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Panel, Nav, NavItem } from 'react-bootstrap';
import './Detailsub.css';
import Navbar from './CustomNavbar';
import Footer from './FooterBottom';
export default class DetailSubscribe extends Component{
    render(){
        return(
                <div>
                    <div className="body-detail">
                        <Navbar/>
                         <div className="container">
                            <h3>Account Info</h3>
                              <Panel>
                                    <Panel.Body>Nama Lengkap : </Panel.Body>
                                    <Panel.Body>Alamat Email : </Panel.Body>
                                    <Panel.Body>Telepon Rumah : </Panel.Body>
                                    <Panel.Body>No Handphone : </Panel.Body>
                              </Panel> 
                               <h3>Home Info</h3>
                              <Panel>
                                    <Panel.Body>Kota  : </Panel.Body>
                                    <Panel.Body>Tipe : </Panel.Body>
                                    <Panel.Body>Lokasi : </Panel.Body>
                                    <Panel.Body>Jalan : </Panel.Body>
                                    <Panel.Body>No/Unit : </Panel.Body>
                              </Panel>
                               <h3>Service Info</h3>
                              <Panel>
                                    <Panel.Body>Customer ID  : </Panel.Body>
                                    <Panel.Body>Paket : </Panel.Body>
                                    <Panel.Body>Promo : </Panel.Body>
                                    <Panel.Body>Harga : </Panel.Body>
                                    <Panel.Body>Status : </Panel.Body>
                              </Panel>
                         </div>
                         <Footer/>
                    </div>
                </div>
            )
    }
}