import React, {Component} from "react";
import './CustomNavbar.css';
import { Link } from 'react-router-dom';
import { nav } from 'react-bootstrap';
import logogroovy from '../assets/img/logo-groovy.png';
import './HeaderLogin.css';

class HeaderLogin extends Component {
    
  render(){
    return(
      
      <header>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <nav id="" className="navbar navbar-expand-lg header-dashboard">
            <div className="container">
                <Link to="/"><img src={logogroovy} alt="logo-groovy" width="120px"/></Link>
            </div>
            </nav>
      </header>
        
    );
  }
}

export default HeaderLogin;
