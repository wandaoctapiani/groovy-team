import React, {Component} from 'react';
import { Table, thead, tbody, Nav, NavItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Detail from '../Detail';
import Navbar from '../CustomNavbar';

export default class BalasLihatTiket extends Component{
    render(){
        return(
          <div>
          <Navbar/>
            <div className="container">
                <h1 className="text-center">Data Tiket</h1>
                    <Table className="table responsive table-bordered table-hover">
                      <thead>
                        <tr style={{background:'#e8e8e8'}}>
                          <th>No</th>
                          <th>ID Tiket</th>
                          <th>Subject</th>
                          <th>Description</th>
                          <th>Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus accusamus Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td><Detail/>
                          </td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur</td>
                          <td><Detail/></td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td><Detail/></td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td><Detail/></td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td><Detail/></td>
                        </tr>
                      </tbody>
                    </Table>
            </div>
        </div>
            );
    }
}