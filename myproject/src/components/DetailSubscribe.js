import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Panel, Nav, NavItem } from 'react-bootstrap';
import Navbar from './CustomNavbar';
import './Detailsub.css';
import LihatDetail from './LihatDetail';

export default class DetailSubscribe extends Component{
    render(){
        return(
                <div>
                <Navbar/>
                    <div className="jumbotron">
                     <LihatDetail/>
                        <Nav>
                      <a>
                        <NavItem eventKey={1} componentClass={Link} href="/" to="/hal-sub">
                        Home
                        </NavItem>
                      </a>
                  </Nav>
                    </div>
                    
                </div>
            )
    }
}