import React, {Component} from 'react';
import { Table, thead, tbody, Nav, NavItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Navbar from './CustomNavbar';
import Footer from './FooterBottom';

export default class LihatKeuangan extends Component{
    render(){
        return(
          <div>
              <Navbar/>
                <div className="container">
                  <h1 className="text-center">Data Keuangan</h1>
                    <Table className="table responsive table-bordered">
                      <thead>
                        <tr className="bg-danger" style={{background:'#e8e8e8'}}>
                          <th>No</th>
                          <th>Customer ID</th>
                          <th>Nama Customer</th>
                          <th>Promo/Paket</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td>Aktif</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td>Aktif</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td>Aktif</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Table cell</td>
                          <td> Anim pariatur cliche reprehenderit, enim eiusmod high life
                              accusamus.</td>
                          <td>Aktif</td>
                        </tr>
                      </tbody>
                    </Table>
              </div>
              <Footer/>
        </div>
            );
    }
}