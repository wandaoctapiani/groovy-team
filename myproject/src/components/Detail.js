import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal} from 'react-bootstrap';


export default class Detail extends Component{
  constructor(props, context) {
    super(props, context);

    this.handleHide = this.handleHide.bind(this);

    this.state = {
      show: false
    };
  }

  handleHide() {
    this.setState({ show: false });
  }
    render(){
        return(
           <div>
              <div className="modal-container">
                <Button
                  bsStyle="primary"
                  bsSize="small"
                  onClick={() => this.setState({ show: true })}
                >
                  Detail
                </Button>
                  <Modal
                        show={this.state.show}
                        onHide={this.handleHide}
                        container={this}
                        aria-labelledby="contained-modal-title"
                      >
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title">
                              Lihat Detail
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <p> ID Tiket : </p> 
                            <p> Customer : </p>
                            <p> Subject : </p>
                            <p> Description : </p>
                        </Modal.Body>
                  </Modal>
              </div>
          </div>
           
            )
    }
}