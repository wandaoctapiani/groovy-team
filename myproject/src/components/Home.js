import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { NavItem } from 'react-bootstrap';
import './Home.css';
export default class Home extends Component{
    render(){
        return(
            <div>
            <h1 className="text-center">Silahkan Pilih</h1>
                <div className="container container-fluid">
                    <div className="row">
                            <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                    <i class="far fa-map" aria-hidden="true"></i>
                                        <h2>Tiket</h2>
                                     </div>
                                     <Link to="/tiket">Lihat Tiket</Link>
                                </div>
                            </div>
                            
                             <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                     <i class="fas fa-user-friends" aria-hidden="true"></i>
                                        <h2>Subscribe</h2>
                                     </div>
                                     <Link to="/lihat-subscribe">Lihat Subscribe</Link>
                                </div>
                            </div>
        
                             <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                    <i class="fas fa-paste" aria-hidden="true"></i>
                                        <h2>Keuangan</h2>
                                     </div>
                                     <Link to="/lihat-keuangan">Lihat Keuangan</Link>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            );
    }
}