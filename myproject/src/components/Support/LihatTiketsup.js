import React, {Component} from 'react';
import { Nav, NavItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Navbar from '../CustomNavbar';
import Tabeltiket from '../TableTiket';
import FooterBottom from '../FooterBottom';

export default class LihatTiketsup extends Component{
    render(){
        return(
                <div>
                    <Navbar/>
                    <Tabeltiket/>
                    <FooterBottom/>
                </div>
            )
    }
}