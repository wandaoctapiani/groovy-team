import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';
import LihatDetail from '../LihatDetail';
import Navbar from '../CustomNavbar';

export default class TableLihatDetail extends Component{
    render(){
        return(
        <div>
            <div className="">
                <LihatDetail/>
                <Nav>
                  <a>
                    <NavItem eventKey={1} componentClass={Link} href="/" to="/hal-sub">
                    Home
                    </NavItem>
                  </a>
              </Nav>
            </div>
        </div>
            )
    }
}