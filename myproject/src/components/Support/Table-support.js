import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Table } from 'react-bootstrap';

export default class TableSupport extends Component{
    render(){
        return(
            <div>
               <div className="container">
               <h1 className="text-center">Data Subcribe/Customer</h1>
                    <Table responsive bordered hover>
                      <thead>
                        <tr className="bg-danger" style={{background:'#e8e8e8'}}>
                          <th>No</th>
                          <th>Customer ID</th>
                          <th>Nama</th>
                          <th>Paket</th>
                          <th>Status</th>
                          <th>Lihat Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Budi</td>
                          <td>Level 1</td>
                          <td>Aktif</td>
                          <td>
                           <Link to="/table-lihat">Lihat Detail</Link>
                          </td>
                        </tr>
                      </tbody>
                    </Table>
              </div>
            </div>
            );
    }
}