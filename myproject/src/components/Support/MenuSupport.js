import React, {Component} from 'react';
import { Link } from 'react-router-dom';

export default class HomeCS extends Component{
    render(){
        return(
            <div>
            <h1 className="text-center">Selamat Datang Support</h1>
                <div className="container container-fluid">
                    <div className="row">
                            <div className="col-md-2"></div>
                            <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                    <i class="far fa-map" aria-hidden="true"></i>
                                        <h2>Tiket</h2>
                                     </div>
                                     <Link to="/lihatTiketsup">Lihat Tiket</Link>
                                </div>
                            </div>
                            
                             <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                     <i class="fas fa-user-friends" aria-hidden="true"></i>
                                        <h2>Subscribe</h2>
                                     </div>
                                     <Link to="/hal-sub">Lihat Subscribe</Link>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            );
    }
}