import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';
import Tablesupport from './Table-support';
import Navbar from '../CustomNavbar';

export default class HalSub extends Component{
    render(){
        return(
            <div>
            <Navbar/>
                <Tablesupport/>
            </div>
            )
    }
}