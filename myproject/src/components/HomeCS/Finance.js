import React, {Component} from 'react';
import { Table, thead, tbody, Nav, NavItem } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import TabelFinance from './TableFinance';
import Navbar from '../CustomNavbar';
import './lihatsubedit.css';

export default class Finance extends Component{
    render(){
        return(
          <div>
            <Navbar/>
                <TabelFinance/>
        </div>
            );
    }
}