import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import './lihatsubedit.css';
export default class HomeCS extends Component{
    render(){
        return(
            <div>
            <h1 className="text-center">Selamat Datang Customer Service</h1>
                <div className="container container-fluid">
                    <div className="row">
                            <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                    <i class="far fa-map" aria-hidden="true"></i>
                                        <h2>Tiket</h2>
                                     </div>
                                     <Link to="/lihattiket">Lihat Tiket</Link>
                                </div>
                            </div>
                            
                             <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                     <i class="fas fa-user-friends" aria-hidden="true"></i>
                                        <h2>Subscribe</h2>
                                     </div>
                                     <Link to="/lihatsubedit">Lihat Subscribe</Link>
                                </div>
                            </div>
                            
                            <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                    <i class="fas fa-dollar-sign"></i>
                                        <h2>Finance/Billing</h2>
                                     </div>
                                     <Link to="/finance">Finance/Billing</Link>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            );
    }
}