import React, {Component} from 'react';
import { Table, thead, ButtonToolbar, Nav, NavItem, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Navbar from '../CustomNavbar';
import './lihatsubedit.css';

export default class LihatSubEdit extends Component{
    render(){
        return(
                <div>
                <Navbar/>
                    
                    <div className="container">
                        <h1 className="text-center">Data Subcribe/Customer</h1>
                    <Table responsive bordered hover>
                      <thead>
                        <tr className="bg-danger" style={{background:'#e8e8e8'}}>
                          <th>No</th>
                          <th>Customer ID</th>
                          <th>Nama</th>
                          <th>Paket</th>
                          <th>Status</th>
                          <th>Lihat Detail</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Budi</td>
                          <td>Level 1</td>
                          <td>Aktif</td>
                          <td>
                          <ButtonToolbar>
                           <Button 
                           bsStyle="primary" 
                           bsSize="small">
                              <Link to="/SubscribeCS" style={{color:'#fff'}}>Detail</Link>
                           </Button>
                           <Button 
                           bsStyle="danger" 
                           bsSize="small">
                              <Link to="/SubscribeCS" style={{color:'#fff'}}>Edit</Link>
                           </Button>
                           </ButtonToolbar>
                          </td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Budi</td>
                          <td>Level 1</td>
                          <td>Aktif</td>
                          <td>
                          <ButtonToolbar>
                           <Button 
                           bsStyle="primary" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Detail</Link>
                           </Button>
                           <Button 
                           bsStyle="danger" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Edit</Link>
                           </Button>
                           </ButtonToolbar>
                          </td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Budi</td>
                          <td>Level 1</td>
                          <td>Aktif</td>
                          <td>
                          <ButtonToolbar>
                           <Button 
                           bsStyle="primary" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Detail</Link>
                           </Button>
                           <Button 
                           bsStyle="danger" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Edit</Link>
                           </Button>
                           </ButtonToolbar>
                          </td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Budi</td>
                          <td>Level 1</td>
                          <td>Aktif</td>
                          <td>
                          <ButtonToolbar>
                           <Button 
                           bsStyle="primary" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Detail</Link>
                           </Button>
                           <Button 
                           bsStyle="danger" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Edit</Link>
                           </Button>
                           </ButtonToolbar>
                          </td>
                        </tr>
                         <tr>
                          <td>1</td>
                          <td>11505269</td>
                          <td>Budi</td>
                          <td>Level 1</td>
                          <td>Aktif</td>
                          <td>
                          <ButtonToolbar>
                           <Button 
                           bsStyle="primary" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Detail</Link>
                           </Button>
                           <Button 
                           bsStyle="danger" 
                           bsSize="small">
                              <Link to="/" style={{color:'#fff'}}>Edit</Link>
                           </Button>
                           </ButtonToolbar>
                          </td>
                        </tr>
                      </tbody>
                    </Table>
              </div>
                </div>
            )
    }
}