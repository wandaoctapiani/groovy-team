import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';
import LihatDetail from '../LihatDetail';
import '../Detailsub.css';

export default class HalSub extends Component{
    render(){
        return(
            <div>
                 <LihatDetail/>
                    <Nav>
                          <a>
                            <NavItem eventKey={1} componentClass={Link} href="/" to="/table-billing">
                            Kembali
                            </NavItem>
                          </a>
                    </Nav>
            </div>
            )
    }
}