import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Nav, NavItem } from 'react-bootstrap';
import LihatDetail from '../LihatDetail';
import Navbar from '../CustomNavbar';

export default class HalSub extends Component{
    render(){
        return(
            <div>
            <Navbar/>
            <LihatDetail/>
            </div>
            )
    }
}