import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import './MenuBilling.css'

export default class MenuBilling extends Component{
    render(){
        return(
            <div>
            <h1 className="text-center">Selamat Datang Billing</h1>
                <div className="container container-fluid">
                    <div className="row">
                            <div className="col-md-2"></div>
                                <div className="col-sm-4">
                                    <div className="card text-center">
                                         <div class="title">
                                      <i class="fas fa-dollar-sign"></i>
                                            <h2>Finance/Billing</h2>
                                         </div>
                                         <Link to="/table-billing">Finance/Billing</Link>
                                    </div>
                                </div>
                            
                             <div className="col-sm-4">
                                <div className="card text-center">
                                     <div class="title">
                                     <i class="fas fa-user-friends" aria-hidden="true"></i>
                                        <h2>Subscribe</h2>
                                     </div>
                                     <Link to="/table-billing">Lihat Subscribe</Link>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            );
    }
}