import React, { Component } from 'react';
import './Footer.css';

class FooterBottom extends Component {
  render() {
    return (
      <div className="Footer">
        <footer>
          <div className="footer-bottom">
            <div className="container">
              <span>&copy; 2017-2018, PT. Media Antar Nusa</span>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default FooterBottom;
