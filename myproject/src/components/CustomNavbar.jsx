import React, {Component} from "react";
import './CustomNavbar.css';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem, MenuItem, NavDropdown  } from 'react-bootstrap';
import logogroovy from '../assets/img/logo-groovy.png';

class HeaderDashboard extends Component {
    
  render(){
    return(
      
      <Navbar default collapseOnSelect>
  <Navbar.Header>
    <Navbar.Brand>
        <Link to="/"><img src={logogroovy} alt="logo-groovy" width="120px"/></Link>
    </Navbar.Brand>
    <Navbar.Toggle />
  </Navbar.Header>
  <Navbar.Collapse>
    <Nav>
      <NavItem eventKey={1} componentClass={Link} href="/tiket" to="/tiket">
        Tiket
      </NavItem>
      <NavItem eventKey={2} componentClass={Link} href="/lihat-subscribe" to="/lihat-subscribe">
        Subscribe
      </NavItem>
      <NavItem eventKey={2} componentClass={Link} href="/lihat-keuangan" to="/lihat-keuangan">
        Keuangan
      </NavItem>
      <NavItem eventKey={2} componentClass={Link} href="/table-billing" to="/table-billing">
        Billing
      </NavItem>
      <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
        <MenuItem eventKey={3.1}>Action</MenuItem>
        <MenuItem eventKey={3.2}>Another action</MenuItem>
        <MenuItem eventKey={3.3}>Something else here</MenuItem>
        <MenuItem divider />
        <MenuItem eventKey={3.3}>Separated link</MenuItem>
      </NavDropdown>
    </Nav>
    <Nav pullRight>
         <NavItem eventKey={1} href="#" componentClass={Link} to="/">
                <a className="ml-auto text-grey" style={{fontSize:20}}>
                <i className="fas fa-bell"></i></a>
          </NavItem>
          <NavItem eventKey={2} href="#" componentClass={Link} to="/">
             <a className="ml-auto text-grey" style={{fontSize:15}}>
             <i className="fas fa-power-off"></i> 
             <span className="mobile-hide-content"> Sign Out</span></a>
          </NavItem>
    </Nav>
  </Navbar.Collapse>
</Navbar>
        
    );
  }
}

export default HeaderDashboard;