import React, {Component} from 'react';
import './Login.css';
import LoginCard from './LoginCard';
import FooterBottom from './FooterBottom';
import ImgHomepage1 from '../assets/img/img-homepage.png';
import {Panel, Button} from 'react-bootstrap';
import Header from './HeaderLogin';
import axios from 'axios';
import { Link } from 'react-router-dom';
import MetaTags from 'react-meta-tags';
class Login extends Component {
  
  state = {
    username: '',
    password: ''
  }
  componentDidMount(){
    document.title = "Login"
    
  }
  handleUsername = event => {
    this.setState({username: event.target.value })
  }
  handlePassword = event => {
    this.setState({password: event.target.value })
    // console.log(event)
  }
  
  handleLogin = event => {
    event.preventDefault();

    const login ={
      username: this.state.username,
      password: this.state.password
    };
    
    axios.get(`http://localhost:3000/pegawai`, login, {
    withCredentials: true,
      credentials: 'include' 
    })
      .then(response => {
        if(response.data.code === 409){
          // console.log("oke")
        } else {
          window.location.href = '/tiket';
        }
        
        // if(response.data.code === 202){
        //   window.location.href = `/`;
        // } else { 
        // console.log(response.data.message);
        // document.getElementById("message").innerHTML = response.data.message;
        // } 
      })
  }
  
  
  render(){
    return(
      <div>
      <Header/>
      <MetaTags>
              
              <meta name="robots" content="noindex" />
              <meta name="googlebot" content="noindex" />
              
            </MetaTags>
      <form onSubmit={this.handleLogin}>
        <div className="login-body">
            <div className="container">
                <div className="row">
                <form onSubmit={this.handleLogin}>
                     <div className="box">
                         <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label for="exampleInputEmail1">Email Address or Mobile Phone</label>
                                <input type="text" name="username" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required placeholder="Email Address or Mobile Phone" onChange={this.handleUsername}/>
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            
                            <div className="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" name="password" className="form-control" id="exampleInputPassword1" placeholder="Password" onChange={this.handlePassword}/>
                            </div>
                            <button className="btn btn-login">Login</button>
                                <div className="border-tb" style={{'margin': 50}}>
                                  <Link to="/forgot-login" className="text-grey link">Forgot Password ?</Link>
                                </div>
                        </div>
                      </div>
                </form>
                            
                </div>
            </div>
        </div>
     </form>       
      </div>
      
      );
  }
}

export default Login;