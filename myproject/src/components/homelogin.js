import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import axios from 'axios';


export default class PersonList extends React.Component {
  state = {
    name: '',
  }

  handleChange = event => {
    this.setState({ name: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();

    const login = {
      name: this.state.name
    };

    axios.post(`https://api-groovy-nurhandiyudi.c9users.io/emp/login`, { login })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Person Name:
            <input type="text" name="name" onChange={this.handleChange} />
          </label>
          <button type="submit">Add</button>
        </form>
      </div>
    )
  }
}