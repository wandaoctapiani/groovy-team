import React, { Component } from 'react';
import './Footer.css';
import logogroovywhite from '../assets/img/logo-groovy-white-transparent.png';

class Footer extends Component {
  render() {
    return (
      <div className="Footer">
        <footer>
          <div className="footer-bottom">
              <span>@2017-2018, PT. Media Antar Nusa</span>
          </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
