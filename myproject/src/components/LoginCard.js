import React, {Component} from 'react';
import './Login.css';
import { Link } from "react-router-dom";
import { Button } from 'react-bootstrap';
import logogroovy from '../assets/img/logo-groovy.png';

class LoginCard extends Component {
    
    render(){
        return(
            <div className="row login-card">
            <div className="col-12 text-center">
              <div className="form-group" style={{padding:'2rem 0'}}>
                <img src={logogroovy} alt="logo-groovy" width={130} />
              </div>
            </div>
            <div className="col-12 text-center" style={{'margin-bottom':'1rem'}}>
              <form onSubmit={this.handleLogin}>
                <div className="form-group">
                  <input type="text" className="form-login" name="mailphone" aria-describedby="mailphone" placeholder="Email or Phone Number" onChange={this.handleMailphone} />
                </div>
                <div className="form-group">
                  <input type="password" className="form-login" name="password" placeholder="Password" onChange={this.handlePassword}/>
                </div>
                <p id="message" className="message-danger"></p>
                <div className="form-group form-check text-left">
                  <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                  <label className="form-check-label text-grey" for="exampleCheck1"> Stay Sign In</label>
                </div>
                <div className="form-group">
                  <div class="g-recaptcha" data-sitekey="6Lff-xsTAAAAAJdL8Bv14tF5_KLm_lP8Na2JlwB-" style={{width:'100%'}}></div>
                </div>
                <div className="form-group">
                  <input type="submit" id="" className="groovy-button-grad" value="Submit" />
                </div>
               
              </form>
            </div>
            <div className="col-12" style={{'margin-bottom':'1rem'}}>
              <div className="border-tb">
                <Link to="/login/forgot-password" className="text-grey link">Forgot Password ?</Link>
              </div>
            </div>
            <div className="col-12">
              <p>
                By signin in, you agree to our<br />
                <Link to="">Term of Service</Link> and <Link to="">Privacy Policy</Link>
              </p>
            </div>
          </div>
            );
    }    
    
}

export default LoginCard