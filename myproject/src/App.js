import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Login from './components/Login';
import Home from './components/Home';
import Tiket from './components/Tiket';
import LihatSub from './components/Lihat-Sub';
import Detailsub from './components/Detailsub';
import LihatKeuangan from './components/Lihat-keuangan';
import HomeCS from './components/HomeCS/HomeCS';
import LihatSubEdit from './components/HomeCS/lihatsubedit';
import Finance from './components/HomeCS/Finance';
import LihatTiket from './components/HomeCS/LihatTiket';
import LihatTiketsup from './components/Support/LihatTiketsup';
import MenuSupport from './components/Support/MenuSupport';
import HalSub from './components/Support/hal-sub';
import DetailSubscribe from './components/DetailSubscribe';
import TableSubscribe from './components/TableSubscribe';
import Tablesupport from './components/Support/Table-support';
import MenuBilling from './components/Billing/MenuBilling';
import TableBilling from './components/Billing/LihatDetail';
import Tablebiling from './components/Billing/Tablebiling';
import LihatDetail from './components/LihatDetail';
import TableDetail from './components/Billing/TableDetail';
import TableLihatDetail from './components/Support/TableLihatDetail';
import SubCS from './components/HomeCS/subCS';
import MenuHelpDesk from './components/HelpDesk/MenuHelpDesk';
import BalasLihatTiket from './components/HelpDesk/BalasLihatTiket';
import Datatable from './components/Tbl';
import HomeLogin from './components/homelogin';
import Latihan from './components/latihan';
import LatState from './components/latState';


class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Login}/>
          <Route exact path="/tiket" component={Tiket}/>
          <Route exact path="/lihat-subscribe" component={LihatSub}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/detailsub" component={Detailsub}/>
          <Route exact path="/lihat-keuangan" component={LihatKeuangan}/>
          <Route exact path="/HomeCS" component={HomeCS}/>
          <Route exact path="/lihatsubedit" component={LihatSubEdit}/>
          <Route exact path="/finance" component={Finance}/>
          <Route exact path="/lihattiket" component={LihatTiket}/>
          <Route exact path="/lihatTiketsup" component={LihatTiketsup}/>
          <Route exact path="/menu-support" component={MenuSupport}/>
          <Route exact path="/hal-sub" component={HalSub}/>
          <Route exact path="/detail-subscribe" component={DetailSubscribe}/>
          <Route exact path="/table-subscribe" component={TableSubscribe}/>
          <Route exact path="/table-support" component={Tablesupport}/>
          <Route exact path="/menu-billing" component={MenuBilling}/>
          <Route exact path="/table-billing" component={TableBilling}/>
          <Route exact path="/subscribe" component={Tablebiling}/>
          <Route exact path="/lihat-detail" component={LihatDetail}/>
          <Route exact path="/table-detail" component={TableDetail}/>
          <Route exact path="/SubscribeCS" component={SubCS}/>
          <Route exact path="/table-lihat" component={TableLihatDetail}/>
          <Route exact path="/menu-help-desk" component={MenuHelpDesk}/>
          <Route exact path="/lihat-tiket" component={BalasLihatTiket}/>
          <Route exact path="/datatable" component={Datatable}/>
          <Route exact path="/homelogin" component={HomeLogin}/>
          <Route exact path="/latihan" component={Latihan}/>
          <Route exact path="/StateComponent" component={LatState}/>
        
        </div> 
        
      </Router>
     
    );
  }
}
 
export default App;
